from confluent_kafka import Producer


def delivery_report(err, msg):
    if err is not None:
        print(f"Message delivery failed: {err}")
    else:
        print(f"Message delivered to {msg.topic()} [{msg.partition()}]")


def run_extended_producer(producer, topic):
    for i in range(10):
        producer.produce(topic, key=str(i), value=f'message {i}', callback=delivery_report)


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'client.id': 'my-producer',  # Optional client ID used for logging / metrics
        'batch.size': 65536,  # Larger batch size for improved throughput
        'linger.ms': 100,  # Wait time before sending a batch
        'retries': 5,  # Number of retries in case of failure
        'retry.backoff.ms': 200  # Wait time between retries
    }
    producer = Producer(conf)
    run_extended_producer(producer, 'combined_topic')
    producer.flush()
