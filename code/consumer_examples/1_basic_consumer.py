from confluent_kafka import Consumer


def run_basic_consumer(consumer, topic):
    consumer.subscribe([topic])
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            if msg.error():
                print(f"Consumer error: {msg.error()}")
            else:
                print(f"Received message: {msg.key()}:{msg.value()} [partition {msg.partition()}]")
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'basic-consumer-group',
        # 'earliest' will start from the beginning of the topic, while 'latest' will only receive new messages
        'auto.offset.reset': 'earliest'
    }
    consumer = Consumer(conf)
    run_basic_consumer(consumer, 'basic_topic')
