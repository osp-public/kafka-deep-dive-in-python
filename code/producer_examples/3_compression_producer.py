from confluent_kafka import Producer


def run_compression_producer(producer, topic):
    for i in range(10):
        producer.produce(topic, key=str(i), value=f'message {i}')


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'compression.type': 'gzip'
    }
    producer = Producer(conf)
    run_compression_producer(producer, 'compression_topic')
    producer.flush()
