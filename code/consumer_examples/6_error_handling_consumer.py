from confluent_kafka import Consumer, KafkaError


def run_error_handling_consumer(consumer, topic):
    consumer.subscribe([topic])
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            if msg.error():
                error = msg.error()
                if error.code() == KafkaError._PARTITION_EOF:
                    print(f"Reached end of partition {msg.partition()} at offset {msg.offset()}")
                else:
                    print(f"Consumer error: {error}")
            else:
                print(f"Received message: {msg.key()}:{msg.value()} [partition {msg.partition()}]")
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'error-handling-consumer-group',
        'auto.offset.reset': 'earliest',
        'enable.partition.eof': True  # Disabled by default!
    }
    consumer = Consumer(conf)
    run_error_handling_consumer(consumer, 'basic_topic')
