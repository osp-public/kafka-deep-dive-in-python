from confluent_kafka import Producer


def run_idempotent_producer(producer, topic):
    for i in range(10):
        producer.produce(topic, key=str(i), value=f'message {i}')


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'enable.idempotence': True,  # Enable idempotent delivery
        'acks': 'all',  # Require acknowledgement from all replicas
        'max.in.flight.requests.per.connection': 1  # Allow only one in-flight request per connection
    }
    producer = Producer(conf)
    run_idempotent_producer(producer, 'idempotent_topic')
    producer.flush()
