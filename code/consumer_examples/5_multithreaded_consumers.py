import threading
from confluent_kafka import Consumer


def consumer_thread(topic, conf):
    consumer = Consumer(conf)
    consumer.subscribe([topic])

    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            if msg.error():
                print(f"Consumer error: {msg.error()}")
            else:
                print(f"Received message: {msg.key()}:{msg.value()} [partition {msg.partition()}]")
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'consumer-group-example',
        'auto.offset.reset': 'earliest'
    }

    num_consumers = 3
    threads = []

    for i in range(num_consumers):
        t = threading.Thread(target=consumer_thread, args=('basic_topic', conf))
        t.start()
        threads.append(t)

    try:
        for t in threads:
            t.join()
    except KeyboardInterrupt:
        print("Shutting down consumers...")
