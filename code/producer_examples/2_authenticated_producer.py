import os
from pathlib import Path

from confluent_kafka import Producer
from dotenv import load_dotenv

env_path = Path('..') / '.env'
load_dotenv(dotenv_path=env_path)


def run_authenticated_producer(producer, topic):
    for i in range(10):
        producer.produce(topic, key=str(i), value=f'message {i}')


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': os.getenv('KAFKA_BOOTSTRAP_SERVER'),
        'security.protocol': 'SASL_SSL',
        'sasl.mechanisms': 'PLAIN',
        'sasl.username': os.getenv('KAFKA_SASL_USERNAME'),
        'sasl.password': os.getenv('KAFKA_SASL_PASSWORD')
    }
    producer = Producer(conf)
    run_authenticated_producer(producer, 'kafka.deep.dive.noschema')
    producer.flush()
