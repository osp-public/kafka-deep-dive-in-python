from confluent_kafka import Producer


def run_basic_producer(producer, topic):
    for i in range(10):
        producer.produce(topic, key=str(i), value=f'message {i}')


if __name__ == "__main__":
    conf = {'bootstrap.servers': 'localhost:9092'}
    producer = Producer(conf)
    run_basic_producer(producer, 'basic_topic')
    producer.flush()
