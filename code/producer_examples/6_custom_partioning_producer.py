from confluent_kafka import Producer


def custom_partitioner(topic, key_bytes, all_partitions, available_partitions):
    # Decode the key bytes and convert it to an integer
    key = int(key_bytes.decode('utf-8'))

    # Calculate the partition number based on the key
    partition = key % len(all_partitions)
    return partition


def run_partitioning_producer(producer, topic):
    for i in range(10):
        producer.produce(topic, key=str(i), value=f'message {i}', partitioner=custom_partitioner)


if __name__ == "__main__":
    conf = {'bootstrap.servers': 'localhost:9092'}
    producer = Producer(conf)
    run_partitioning_producer(producer, 'partitioning_topic')
    producer.flush()
