import os
from pathlib import Path

from confluent_kafka import Producer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import SerializationContext, MessageField
from dotenv import load_dotenv

env_path = Path('..') / '.env'
load_dotenv(dotenv_path=env_path)

value_schema_str = """
{
  "doc": "Ein Schema für die Kafka Deep Dive Session",
  "fields": [
    {
      "doc": "The int type is a 32-bit signed integer.",
      "name": "int_field",
      "type": "int"
    },
    {
      "doc": "The double type is a double precision (64-bit) IEEE 754 floating-point number.",
      "name": "double_field",
      "type": "double"
    },
    {
      "doc": "The string is a unicode character sequence.",
      "name": "string_field",
      "type": "string"
    }
  ],
  "name": "Beispielschema",
  "namespace": "kafka.deep.dive",
  "type": "record"
}
"""


def run_avro_producer(producer, topic, value_serializer):
    for i in range(10):
        value = {
            'int_field': i,
            'double_field': float(i),
            'string_field': f'message {i}'
        }
        ctx = SerializationContext(topic, MessageField.VALUE)
        serialized_value = value_serializer(value, ctx)
        producer.produce(topic, key=str(i), value=serialized_value)


if __name__ == "__main__":
    conf = {
        'url': os.getenv('SCHEMA_REGISTRY_URL'),
        'basic.auth.user.info': f"{os.getenv('SCHEMA_REGISTRY_USERNAME')}:{os.getenv('SCHEMA_REGISTRY_PASSWORD')}"
    }

    schema_registry_client = SchemaRegistryClient(conf)
    value_serializer = AvroSerializer(schema_registry_client, value_schema_str)

    producer_conf = {
        'bootstrap.servers': os.getenv('KAFKA_BOOTSTRAP_SERVER'),
        'security.protocol': 'SASL_SSL',
        'sasl.mechanisms': 'PLAIN',
        'sasl.username': os.getenv('KAFKA_SASL_USERNAME'),
        'sasl.password': os.getenv('KAFKA_SASL_PASSWORD'),
    }

    producer = Producer(producer_conf)
    run_avro_producer(producer, 'kafka.deep.dive.withschema', value_serializer)

    producer.flush()
