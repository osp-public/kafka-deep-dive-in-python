# Kafka Deep Dive Workshop

This repository contains code examples and explanations for a Kafka deep dive workshop. 
The examples cover various Kafka producer and consumer scenarios, including basic message production and consumption, 
custom partitioning, idempotent producers, and transactional producers.

## Prerequisites

Before running the examples, ensure that you have the following prerequisites:

- Docker and Docker Compose installed on your machine
- Python 3.6 or later
- Pip (Python package manager) installed

You can also run this without Docker / Local Kafka, but you will need to adjust the samples as shown in `producer_examples/2_authenticated_producer.py`

## Setup

1. Clone this repository:

```bash
git clone git@gitlab.com:osp-public/kafka-deep-dive-in-python.git
cd kafka-deep-dive-in-python
```

2. Install the required Python packages:

```bash
pip install -r requirements.txt
```

3. Copy the `sample.env` file to `.env` and set the required environment variables:

```bash
cp sample.env .env
```

Update the `.env` file with the necessary values for your environment.

4. Start the Kafka and Zookeeper containers using Docker Compose:

```bash
docker-compose up -d
```

## Running the Examples

The code examples are organized into two directories: `producer_examples` and `consumer_examples`. To run an example, 
navigate to the corresponding directory and execute the Python script.

For example, to run the basic producer example:

```bash
cd producer_examples
python basic_producer.py
```

## Helpful Snippets

Consume messages from a topic using Kafka console consumer
To consume messages from a topic using the Kafka console consumer, you can use the following commands:

```bash
docker exec -it code-kafka-1 /usr/bin/kafka-console-consumer --bootstrap-server kafka:9092 --topic basic_topic --from-beginning
```

Replace `code-kafka-1` with the name of your Kafka container, and replace `basic_topic`  with the desired topic name