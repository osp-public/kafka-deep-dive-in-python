from confluent_kafka import Producer


def run_transactions_producer(producer, topic):
    producer.init_transactions()
    producer.begin_transaction()

    try:
        for i in range(10):
            producer.produce(topic, key=str(i), value=f'message {i}')
        producer.commit_transaction()
    except Exception as e:
        print(f"Transaction failed: {e}")
        producer.abort_transaction()


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'transactional.id': 'my-transactional-id',
        'enable.idempotence': True,  # Enable idempotent delivery for transactions
        'acks': 'all',  # Require acknowledgement from all replicas
        'max.in.flight.requests.per.connection': 1  # Allow only one in-flight request per connection
    }
    producer = Producer(conf)
    run_transactions_producer(producer, 'transactions_topic')
    producer.flush()
