from confluent_kafka import Consumer


def run_optimized_consumer(consumer, topic):
    consumer.subscribe([topic])
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            if msg.error():
                print(f"Consumer error: {msg.error()}")
            else:
                print(f"Received message: {msg.key()}:{msg.value()} [partition {msg.partition()}]")
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'optimized-consumer-group',
        'auto.offset.reset': 'earliest',
        'enable.auto.commit': True,
        'auto.commit.interval.ms': 5000,
        'max.poll.records': 100,
        'fetch.min.bytes': 1024,
        'fetch.max.wait.ms': 100,
        'max.partition.fetch.bytes': 1048576
    }
    consumer = Consumer(conf)
    run_optimized_consumer(consumer, 'basic_topic')
