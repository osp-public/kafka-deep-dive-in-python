from confluent_kafka import Consumer


def run_manual_commit_consumer(consumer, topic):
    consumer.subscribe([topic])
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            if msg.error():
                print(f"Consumer error: {msg.error()}")
            else:
                print(f"Received message: {msg.key()}:{msg.value()} [partition {msg.partition()}]")
                consumer.commit(asynchronous=False)
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()


if __name__ == "__main__":
    conf = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'manual-commit-consumer-group',
        'auto.offset.reset': 'earliest',
        'enable.auto.commit': False
    }
    consumer = Consumer(conf)
    run_manual_commit_consumer(consumer, 'basic_topic')
