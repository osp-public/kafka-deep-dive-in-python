import os
from pathlib import Path

from confluent_kafka import Consumer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer
from confluent_kafka.serialization import SerializationContext, MessageField
from dotenv import load_dotenv

env_path = Path('..') / '.env'
load_dotenv(dotenv_path=env_path)


def run_avro_consumer(consumer, topic, value_deserializer):
    consumer.subscribe([topic])
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            if msg.error():
                print(f"Consumer error: {msg.error()}")
            else:
                ctx = SerializationContext(topic, MessageField.VALUE)
                value = value_deserializer(msg.value(), ctx)
                print(f"Received message: {msg.key()}:{value} [partition {msg.partition()}]")
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()


if __name__ == "__main__":
    conf = {
        'url': os.getenv('SCHEMA_REGISTRY_URL'),
        'basic.auth.user.info': f"{os.getenv('SCHEMA_REGISTRY_USERNAME')}:{os.getenv('SCHEMA_REGISTRY_PASSWORD')}"
    }
    schema_registry_client = SchemaRegistryClient(conf)
    value_deserializer = AvroDeserializer(schema_registry_client)

    consumer_conf = {
        'bootstrap.servers': os.getenv('KAFKA_BOOTSTRAP_SERVER'),
        'group.id': 'kafka-deep-dive-avro-consumer-group',
        'security.protocol': 'SASL_SSL',
        'sasl.mechanisms': 'PLAIN',
        'sasl.username': os.getenv('KAFKA_SASL_USERNAME'),
        'sasl.password': os.getenv('KAFKA_SASL_PASSWORD'),
        'auto.offset.reset': 'earliest'
    }

    consumer = Consumer(consumer_conf)
    run_avro_consumer(consumer, 'kafka.deep.dive.withschema', value_deserializer)
